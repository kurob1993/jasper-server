FROM docker.io/bitnami/jasperreports:latest
COPY ./config/ki.keystore /opt/bitnami/tomcat/conf/ki.keystore
COPY ./config/server.xml /opt/bitnami/tomcat/conf/server.xml