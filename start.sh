docker-compose down && docker-compose build && docker-compose up -d
echo "Building JasperReports Server..."
#Long waiting period to ensure the container is up and running (health checks didn't worked out well)
sleep 180;
echo "...completed!"
docker cp viewReport/ViewReport.jsp jasper-server-jasperreports-1:/opt/bitnami/jasperreports/WEB-INF/jsp/modules/viewReport/ViewReport.jsp
echo "Ready to rock!"